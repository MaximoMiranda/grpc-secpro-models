// Code generated by protoc-gen-go. DO NOT EDIT.
// source: protos/accounts-service.proto

package secprosvc

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	any "github.com/golang/protobuf/ptypes/any"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// User Object ...
type User struct {
	ID                   int64    `protobuf:"varint,1,opt,name=ID,proto3" json:"ID,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Lastname             string   `protobuf:"bytes,3,opt,name=lastname,proto3" json:"lastname,omitempty"`
	Phonenumber          string   `protobuf:"bytes,4,opt,name=phonenumber,proto3" json:"phonenumber,omitempty"`
	Email                string   `protobuf:"bytes,5,opt,name=email,proto3" json:"email,omitempty"`
	Password             string   `protobuf:"bytes,6,opt,name=password,proto3" json:"password,omitempty"`
	CreatedAt            int64    `protobuf:"varint,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt            int64    `protobuf:"varint,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *User) Reset()         { *m = User{} }
func (m *User) String() string { return proto.CompactTextString(m) }
func (*User) ProtoMessage()    {}
func (*User) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{0}
}

func (m *User) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_User.Unmarshal(m, b)
}
func (m *User) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_User.Marshal(b, m, deterministic)
}
func (m *User) XXX_Merge(src proto.Message) {
	xxx_messageInfo_User.Merge(m, src)
}
func (m *User) XXX_Size() int {
	return xxx_messageInfo_User.Size(m)
}
func (m *User) XXX_DiscardUnknown() {
	xxx_messageInfo_User.DiscardUnknown(m)
}

var xxx_messageInfo_User proto.InternalMessageInfo

func (m *User) GetID() int64 {
	if m != nil {
		return m.ID
	}
	return 0
}

func (m *User) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *User) GetLastname() string {
	if m != nil {
		return m.Lastname
	}
	return ""
}

func (m *User) GetPhonenumber() string {
	if m != nil {
		return m.Phonenumber
	}
	return ""
}

func (m *User) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *User) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

func (m *User) GetCreatedAt() int64 {
	if m != nil {
		return m.CreatedAt
	}
	return 0
}

func (m *User) GetUpdatedAt() int64 {
	if m != nil {
		return m.UpdatedAt
	}
	return 0
}

// Role Object ...
type Role struct {
	ID                   int64    `protobuf:"varint,1,opt,name=ID,proto3" json:"ID,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Description          string   `protobuf:"bytes,3,opt,name=description,proto3" json:"description,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Role) Reset()         { *m = Role{} }
func (m *Role) String() string { return proto.CompactTextString(m) }
func (*Role) ProtoMessage()    {}
func (*Role) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{1}
}

func (m *Role) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Role.Unmarshal(m, b)
}
func (m *Role) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Role.Marshal(b, m, deterministic)
}
func (m *Role) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Role.Merge(m, src)
}
func (m *Role) XXX_Size() int {
	return xxx_messageInfo_Role.Size(m)
}
func (m *Role) XXX_DiscardUnknown() {
	xxx_messageInfo_Role.DiscardUnknown(m)
}

var xxx_messageInfo_Role proto.InternalMessageInfo

func (m *Role) GetID() int64 {
	if m != nil {
		return m.ID
	}
	return 0
}

func (m *Role) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Role) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

// Error Object ...
type Error struct {
	Code                 string   `protobuf:"bytes,1,opt,name=code,proto3" json:"code,omitempty"`
	Message              string   `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	Data                 *any.Any `protobuf:"bytes,3,opt,name=data,proto3" json:"data,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Error) Reset()         { *m = Error{} }
func (m *Error) String() string { return proto.CompactTextString(m) }
func (*Error) ProtoMessage()    {}
func (*Error) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{2}
}

func (m *Error) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Error.Unmarshal(m, b)
}
func (m *Error) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Error.Marshal(b, m, deterministic)
}
func (m *Error) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Error.Merge(m, src)
}
func (m *Error) XXX_Size() int {
	return xxx_messageInfo_Error.Size(m)
}
func (m *Error) XXX_DiscardUnknown() {
	xxx_messageInfo_Error.DiscardUnknown(m)
}

var xxx_messageInfo_Error proto.InternalMessageInfo

func (m *Error) GetCode() string {
	if m != nil {
		return m.Code
	}
	return ""
}

func (m *Error) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *Error) GetData() *any.Any {
	if m != nil {
		return m.Data
	}
	return nil
}

type PatchUserRequest struct {
	ID                   int64    `protobuf:"varint,1,opt,name=ID,proto3" json:"ID,omitempty"`
	Payload              *User    `protobuf:"bytes,2,opt,name=payload,proto3" json:"payload,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PatchUserRequest) Reset()         { *m = PatchUserRequest{} }
func (m *PatchUserRequest) String() string { return proto.CompactTextString(m) }
func (*PatchUserRequest) ProtoMessage()    {}
func (*PatchUserRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{3}
}

func (m *PatchUserRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PatchUserRequest.Unmarshal(m, b)
}
func (m *PatchUserRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PatchUserRequest.Marshal(b, m, deterministic)
}
func (m *PatchUserRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PatchUserRequest.Merge(m, src)
}
func (m *PatchUserRequest) XXX_Size() int {
	return xxx_messageInfo_PatchUserRequest.Size(m)
}
func (m *PatchUserRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_PatchUserRequest.DiscardUnknown(m)
}

var xxx_messageInfo_PatchUserRequest proto.InternalMessageInfo

func (m *PatchUserRequest) GetID() int64 {
	if m != nil {
		return m.ID
	}
	return 0
}

func (m *PatchUserRequest) GetPayload() *User {
	if m != nil {
		return m.Payload
	}
	return nil
}

type DeleteUserRequest struct {
	ID                   int64    `protobuf:"varint,1,opt,name=ID,proto3" json:"ID,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteUserRequest) Reset()         { *m = DeleteUserRequest{} }
func (m *DeleteUserRequest) String() string { return proto.CompactTextString(m) }
func (*DeleteUserRequest) ProtoMessage()    {}
func (*DeleteUserRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{4}
}

func (m *DeleteUserRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteUserRequest.Unmarshal(m, b)
}
func (m *DeleteUserRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteUserRequest.Marshal(b, m, deterministic)
}
func (m *DeleteUserRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteUserRequest.Merge(m, src)
}
func (m *DeleteUserRequest) XXX_Size() int {
	return xxx_messageInfo_DeleteUserRequest.Size(m)
}
func (m *DeleteUserRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteUserRequest.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteUserRequest proto.InternalMessageInfo

func (m *DeleteUserRequest) GetID() int64 {
	if m != nil {
		return m.ID
	}
	return 0
}

type ValidateUserAccountRequest struct {
	Email                string   `protobuf:"bytes,1,opt,name=email,proto3" json:"email,omitempty"`
	Password             string   `protobuf:"bytes,2,opt,name=password,proto3" json:"password,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ValidateUserAccountRequest) Reset()         { *m = ValidateUserAccountRequest{} }
func (m *ValidateUserAccountRequest) String() string { return proto.CompactTextString(m) }
func (*ValidateUserAccountRequest) ProtoMessage()    {}
func (*ValidateUserAccountRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{5}
}

func (m *ValidateUserAccountRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ValidateUserAccountRequest.Unmarshal(m, b)
}
func (m *ValidateUserAccountRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ValidateUserAccountRequest.Marshal(b, m, deterministic)
}
func (m *ValidateUserAccountRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ValidateUserAccountRequest.Merge(m, src)
}
func (m *ValidateUserAccountRequest) XXX_Size() int {
	return xxx_messageInfo_ValidateUserAccountRequest.Size(m)
}
func (m *ValidateUserAccountRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ValidateUserAccountRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ValidateUserAccountRequest proto.InternalMessageInfo

func (m *ValidateUserAccountRequest) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *ValidateUserAccountRequest) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

type PatchRoleRequest struct {
	ID                   int64    `protobuf:"varint,1,opt,name=ID,proto3" json:"ID,omitempty"`
	Payload              *Role    `protobuf:"bytes,2,opt,name=payload,proto3" json:"payload,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PatchRoleRequest) Reset()         { *m = PatchRoleRequest{} }
func (m *PatchRoleRequest) String() string { return proto.CompactTextString(m) }
func (*PatchRoleRequest) ProtoMessage()    {}
func (*PatchRoleRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{6}
}

func (m *PatchRoleRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PatchRoleRequest.Unmarshal(m, b)
}
func (m *PatchRoleRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PatchRoleRequest.Marshal(b, m, deterministic)
}
func (m *PatchRoleRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PatchRoleRequest.Merge(m, src)
}
func (m *PatchRoleRequest) XXX_Size() int {
	return xxx_messageInfo_PatchRoleRequest.Size(m)
}
func (m *PatchRoleRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_PatchRoleRequest.DiscardUnknown(m)
}

var xxx_messageInfo_PatchRoleRequest proto.InternalMessageInfo

func (m *PatchRoleRequest) GetID() int64 {
	if m != nil {
		return m.ID
	}
	return 0
}

func (m *PatchRoleRequest) GetPayload() *Role {
	if m != nil {
		return m.Payload
	}
	return nil
}

type DeleteRoleRequest struct {
	ID                   int64    `protobuf:"varint,1,opt,name=ID,proto3" json:"ID,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteRoleRequest) Reset()         { *m = DeleteRoleRequest{} }
func (m *DeleteRoleRequest) String() string { return proto.CompactTextString(m) }
func (*DeleteRoleRequest) ProtoMessage()    {}
func (*DeleteRoleRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{7}
}

func (m *DeleteRoleRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteRoleRequest.Unmarshal(m, b)
}
func (m *DeleteRoleRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteRoleRequest.Marshal(b, m, deterministic)
}
func (m *DeleteRoleRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteRoleRequest.Merge(m, src)
}
func (m *DeleteRoleRequest) XXX_Size() int {
	return xxx_messageInfo_DeleteRoleRequest.Size(m)
}
func (m *DeleteRoleRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteRoleRequest.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteRoleRequest proto.InternalMessageInfo

func (m *DeleteRoleRequest) GetID() int64 {
	if m != nil {
		return m.ID
	}
	return 0
}

type UserResponse struct {
	StatusCode           int32    `protobuf:"varint,1,opt,name=statusCode,proto3" json:"statusCode,omitempty"`
	Data                 *User    `protobuf:"bytes,2,opt,name=data,proto3" json:"data,omitempty"`
	Error                *Error   `protobuf:"bytes,3,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserResponse) Reset()         { *m = UserResponse{} }
func (m *UserResponse) String() string { return proto.CompactTextString(m) }
func (*UserResponse) ProtoMessage()    {}
func (*UserResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{8}
}

func (m *UserResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserResponse.Unmarshal(m, b)
}
func (m *UserResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserResponse.Marshal(b, m, deterministic)
}
func (m *UserResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserResponse.Merge(m, src)
}
func (m *UserResponse) XXX_Size() int {
	return xxx_messageInfo_UserResponse.Size(m)
}
func (m *UserResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UserResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UserResponse proto.InternalMessageInfo

func (m *UserResponse) GetStatusCode() int32 {
	if m != nil {
		return m.StatusCode
	}
	return 0
}

func (m *UserResponse) GetData() *User {
	if m != nil {
		return m.Data
	}
	return nil
}

func (m *UserResponse) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

type UsersResponse struct {
	StatusCode           int32    `protobuf:"varint,1,opt,name=statusCode,proto3" json:"statusCode,omitempty"`
	Data                 []*User  `protobuf:"bytes,2,rep,name=data,proto3" json:"data,omitempty"`
	Error                *Error   `protobuf:"bytes,3,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UsersResponse) Reset()         { *m = UsersResponse{} }
func (m *UsersResponse) String() string { return proto.CompactTextString(m) }
func (*UsersResponse) ProtoMessage()    {}
func (*UsersResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{9}
}

func (m *UsersResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UsersResponse.Unmarshal(m, b)
}
func (m *UsersResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UsersResponse.Marshal(b, m, deterministic)
}
func (m *UsersResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UsersResponse.Merge(m, src)
}
func (m *UsersResponse) XXX_Size() int {
	return xxx_messageInfo_UsersResponse.Size(m)
}
func (m *UsersResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UsersResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UsersResponse proto.InternalMessageInfo

func (m *UsersResponse) GetStatusCode() int32 {
	if m != nil {
		return m.StatusCode
	}
	return 0
}

func (m *UsersResponse) GetData() []*User {
	if m != nil {
		return m.Data
	}
	return nil
}

func (m *UsersResponse) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

type UserBoolResponse struct {
	StatusCode           int32    `protobuf:"varint,1,opt,name=statusCode,proto3" json:"statusCode,omitempty"`
	Data                 bool     `protobuf:"varint,2,opt,name=data,proto3" json:"data,omitempty"`
	Error                *Error   `protobuf:"bytes,3,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserBoolResponse) Reset()         { *m = UserBoolResponse{} }
func (m *UserBoolResponse) String() string { return proto.CompactTextString(m) }
func (*UserBoolResponse) ProtoMessage()    {}
func (*UserBoolResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{10}
}

func (m *UserBoolResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserBoolResponse.Unmarshal(m, b)
}
func (m *UserBoolResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserBoolResponse.Marshal(b, m, deterministic)
}
func (m *UserBoolResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserBoolResponse.Merge(m, src)
}
func (m *UserBoolResponse) XXX_Size() int {
	return xxx_messageInfo_UserBoolResponse.Size(m)
}
func (m *UserBoolResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UserBoolResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UserBoolResponse proto.InternalMessageInfo

func (m *UserBoolResponse) GetStatusCode() int32 {
	if m != nil {
		return m.StatusCode
	}
	return 0
}

func (m *UserBoolResponse) GetData() bool {
	if m != nil {
		return m.Data
	}
	return false
}

func (m *UserBoolResponse) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

type RoleResponse struct {
	StatusCode           int32    `protobuf:"varint,1,opt,name=statusCode,proto3" json:"statusCode,omitempty"`
	Data                 *Role    `protobuf:"bytes,2,opt,name=data,proto3" json:"data,omitempty"`
	Error                *Error   `protobuf:"bytes,3,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RoleResponse) Reset()         { *m = RoleResponse{} }
func (m *RoleResponse) String() string { return proto.CompactTextString(m) }
func (*RoleResponse) ProtoMessage()    {}
func (*RoleResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{11}
}

func (m *RoleResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RoleResponse.Unmarshal(m, b)
}
func (m *RoleResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RoleResponse.Marshal(b, m, deterministic)
}
func (m *RoleResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RoleResponse.Merge(m, src)
}
func (m *RoleResponse) XXX_Size() int {
	return xxx_messageInfo_RoleResponse.Size(m)
}
func (m *RoleResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_RoleResponse.DiscardUnknown(m)
}

var xxx_messageInfo_RoleResponse proto.InternalMessageInfo

func (m *RoleResponse) GetStatusCode() int32 {
	if m != nil {
		return m.StatusCode
	}
	return 0
}

func (m *RoleResponse) GetData() *Role {
	if m != nil {
		return m.Data
	}
	return nil
}

func (m *RoleResponse) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

type RolesResponse struct {
	StatusCode           int32    `protobuf:"varint,1,opt,name=statusCode,proto3" json:"statusCode,omitempty"`
	Data                 []*Role  `protobuf:"bytes,2,rep,name=data,proto3" json:"data,omitempty"`
	Error                *Error   `protobuf:"bytes,3,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RolesResponse) Reset()         { *m = RolesResponse{} }
func (m *RolesResponse) String() string { return proto.CompactTextString(m) }
func (*RolesResponse) ProtoMessage()    {}
func (*RolesResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{12}
}

func (m *RolesResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RolesResponse.Unmarshal(m, b)
}
func (m *RolesResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RolesResponse.Marshal(b, m, deterministic)
}
func (m *RolesResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RolesResponse.Merge(m, src)
}
func (m *RolesResponse) XXX_Size() int {
	return xxx_messageInfo_RolesResponse.Size(m)
}
func (m *RolesResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_RolesResponse.DiscardUnknown(m)
}

var xxx_messageInfo_RolesResponse proto.InternalMessageInfo

func (m *RolesResponse) GetStatusCode() int32 {
	if m != nil {
		return m.StatusCode
	}
	return 0
}

func (m *RolesResponse) GetData() []*Role {
	if m != nil {
		return m.Data
	}
	return nil
}

func (m *RolesResponse) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

type RoleBoolResponse struct {
	StatusCode           int32    `protobuf:"varint,1,opt,name=statusCode,proto3" json:"statusCode,omitempty"`
	Data                 bool     `protobuf:"varint,2,opt,name=data,proto3" json:"data,omitempty"`
	Error                *Error   `protobuf:"bytes,3,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RoleBoolResponse) Reset()         { *m = RoleBoolResponse{} }
func (m *RoleBoolResponse) String() string { return proto.CompactTextString(m) }
func (*RoleBoolResponse) ProtoMessage()    {}
func (*RoleBoolResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_e62997a132eb0b18, []int{13}
}

func (m *RoleBoolResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RoleBoolResponse.Unmarshal(m, b)
}
func (m *RoleBoolResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RoleBoolResponse.Marshal(b, m, deterministic)
}
func (m *RoleBoolResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RoleBoolResponse.Merge(m, src)
}
func (m *RoleBoolResponse) XXX_Size() int {
	return xxx_messageInfo_RoleBoolResponse.Size(m)
}
func (m *RoleBoolResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_RoleBoolResponse.DiscardUnknown(m)
}

var xxx_messageInfo_RoleBoolResponse proto.InternalMessageInfo

func (m *RoleBoolResponse) GetStatusCode() int32 {
	if m != nil {
		return m.StatusCode
	}
	return 0
}

func (m *RoleBoolResponse) GetData() bool {
	if m != nil {
		return m.Data
	}
	return false
}

func (m *RoleBoolResponse) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

func init() {
	proto.RegisterType((*User)(nil), "secprosvc.User")
	proto.RegisterType((*Role)(nil), "secprosvc.Role")
	proto.RegisterType((*Error)(nil), "secprosvc.Error")
	proto.RegisterType((*PatchUserRequest)(nil), "secprosvc.PatchUserRequest")
	proto.RegisterType((*DeleteUserRequest)(nil), "secprosvc.DeleteUserRequest")
	proto.RegisterType((*ValidateUserAccountRequest)(nil), "secprosvc.ValidateUserAccountRequest")
	proto.RegisterType((*PatchRoleRequest)(nil), "secprosvc.PatchRoleRequest")
	proto.RegisterType((*DeleteRoleRequest)(nil), "secprosvc.DeleteRoleRequest")
	proto.RegisterType((*UserResponse)(nil), "secprosvc.UserResponse")
	proto.RegisterType((*UsersResponse)(nil), "secprosvc.UsersResponse")
	proto.RegisterType((*UserBoolResponse)(nil), "secprosvc.UserBoolResponse")
	proto.RegisterType((*RoleResponse)(nil), "secprosvc.RoleResponse")
	proto.RegisterType((*RolesResponse)(nil), "secprosvc.RolesResponse")
	proto.RegisterType((*RoleBoolResponse)(nil), "secprosvc.RoleBoolResponse")
}

func init() { proto.RegisterFile("protos/accounts-service.proto", fileDescriptor_e62997a132eb0b18) }

var fileDescriptor_e62997a132eb0b18 = []byte{
	// 591 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xbc, 0x54, 0xdd, 0x6e, 0x12, 0x41,
	0x14, 0x0e, 0x14, 0x0a, 0x1c, 0xaa, 0xc5, 0x69, 0x13, 0x57, 0x6a, 0x0d, 0xd9, 0x46, 0x83, 0x17,
	0x2e, 0x09, 0x26, 0x5e, 0x79, 0x43, 0x8b, 0x69, 0x9a, 0xa8, 0xd1, 0x35, 0xf5, 0xb6, 0x19, 0x76,
	0x8f, 0x94, 0x64, 0xd9, 0x59, 0x67, 0x66, 0x6b, 0x48, 0x7d, 0x50, 0x9f, 0xc2, 0x67, 0x30, 0x73,
	0x66, 0x97, 0x6e, 0x41, 0x9a, 0x96, 0x1a, 0xef, 0x76, 0xce, 0xff, 0xf9, 0xbe, 0xb3, 0x1f, 0xec,
	0x27, 0x52, 0x68, 0xa1, 0x7a, 0x3c, 0x08, 0x44, 0x1a, 0x6b, 0xf5, 0x4a, 0xa1, 0xbc, 0x98, 0x04,
	0xe8, 0x91, 0x9d, 0x35, 0x14, 0x06, 0x89, 0x14, 0xea, 0x22, 0x68, 0x3f, 0x19, 0x0b, 0x31, 0x8e,
	0xb0, 0x47, 0x8e, 0x51, 0xfa, 0xad, 0xc7, 0xe3, 0x99, 0x8d, 0x72, 0x7f, 0x95, 0xa0, 0x72, 0xaa,
	0x50, 0xb2, 0x87, 0x50, 0x3e, 0x19, 0x3a, 0xa5, 0x4e, 0xa9, 0xbb, 0xe1, 0x97, 0x4f, 0x86, 0x8c,
	0x41, 0x25, 0xe6, 0x53, 0x74, 0xca, 0x9d, 0x52, 0xb7, 0xe1, 0xd3, 0x37, 0x6b, 0x43, 0x3d, 0xe2,
	0x4a, 0x93, 0x7d, 0x83, 0xec, 0xf3, 0x37, 0xeb, 0x40, 0x33, 0x39, 0x17, 0x31, 0xc6, 0xe9, 0x74,
	0x84, 0xd2, 0xa9, 0x90, 0xbb, 0x68, 0x62, 0xbb, 0x50, 0xc5, 0x29, 0x9f, 0x44, 0x4e, 0x95, 0x7c,
	0xf6, 0x61, 0x6a, 0x26, 0x5c, 0xa9, 0x1f, 0x42, 0x86, 0xce, 0xa6, 0xad, 0x99, 0xbf, 0xd9, 0x3e,
	0x40, 0x20, 0x91, 0x6b, 0x0c, 0xcf, 0xb8, 0x76, 0x6a, 0x34, 0x5b, 0x23, 0xb3, 0x0c, 0xb4, 0x71,
	0xa7, 0x49, 0x98, 0xbb, 0xeb, 0xd6, 0x9d, 0x59, 0x06, 0xda, 0x7d, 0x0f, 0x15, 0x5f, 0x44, 0x78,
	0xab, 0xcd, 0x3a, 0xd0, 0x0c, 0x51, 0x05, 0x72, 0x92, 0xe8, 0x89, 0x88, 0xb3, 0xe5, 0x8a, 0x26,
	0xf7, 0x0c, 0xaa, 0xef, 0xa4, 0x14, 0xd2, 0xa4, 0x07, 0x22, 0x44, 0x2a, 0xd8, 0xf0, 0xe9, 0x9b,
	0x39, 0x50, 0x9b, 0xa2, 0x52, 0x7c, 0x9c, 0x57, 0xcd, 0x9f, 0xac, 0x0b, 0x95, 0x90, 0x6b, 0x4e,
	0x15, 0x9b, 0xfd, 0x5d, 0xcf, 0x32, 0xe1, 0xe5, 0x4c, 0x78, 0x83, 0x78, 0xe6, 0x53, 0x84, 0xfb,
	0x01, 0x5a, 0x9f, 0xb8, 0x0e, 0xce, 0x0d, 0x1b, 0x3e, 0x7e, 0x4f, 0x51, 0xe9, 0xa5, 0xd1, 0x5f,
	0x42, 0x2d, 0xe1, 0xb3, 0x48, 0xf0, 0x90, 0xfa, 0x34, 0xfb, 0xdb, 0xde, 0x9c, 0x65, 0x8f, 0x12,
	0x73, 0xbf, 0x7b, 0x00, 0x8f, 0x86, 0x18, 0xa1, 0xc6, 0x1b, 0xea, 0xb9, 0x1f, 0xa1, 0xfd, 0x95,
	0x47, 0x13, 0x83, 0x98, 0x09, 0x1b, 0xd8, 0x4b, 0xca, 0xa3, 0xe7, 0x84, 0x95, 0x56, 0x11, 0x56,
	0xbe, 0x4e, 0xd8, 0x7c, 0x07, 0x83, 0xfb, 0x5a, 0x3b, 0x50, 0xe2, 0xf2, 0x0e, 0x37, 0xd4, 0x73,
	0x2f, 0x61, 0xcb, 0xae, 0xa8, 0x12, 0x11, 0x2b, 0x64, 0xcf, 0x00, 0x94, 0xe6, 0x3a, 0x55, 0x47,
	0x39, 0x4b, 0x55, 0xbf, 0x60, 0x61, 0x07, 0x19, 0x23, 0x2b, 0x00, 0x24, 0x27, 0x7b, 0x01, 0x55,
	0x34, 0x6c, 0x67, 0xbc, 0xb5, 0x0a, 0x51, 0x74, 0x05, 0xbe, 0x75, 0xbb, 0x3f, 0xe1, 0x81, 0xc9,
	0x52, 0x6b, 0x74, 0xdf, 0xb8, 0x7f, 0xf7, 0x18, 0x5a, 0x26, 0xeb, 0x50, 0x88, 0xe8, 0xd6, 0x03,
	0xb0, 0xc2, 0xfa, 0xf5, 0x3b, 0xf6, 0xbb, 0x84, 0x2d, 0xcb, 0xc4, 0xbd, 0xa1, 0xa6, 0x32, 0x77,
	0x86, 0xda, 0x64, 0xfd, 0x0b, 0xa8, 0xd7, 0xe8, 0x1e, 0x43, 0xcb, 0x64, 0xfd, 0x2f, 0xa8, 0xfb,
	0xbf, 0xcb, 0xb0, 0x93, 0xfd, 0x8e, 0xca, 0x70, 0xfc, 0xc5, 0x6a, 0x3b, 0x7b, 0x03, 0x70, 0x44,
	0x02, 0x48, 0xa2, 0xbd, 0x78, 0x3f, 0xed, 0xc7, 0x8b, 0x07, 0x95, 0xcf, 0xfa, 0x16, 0xb6, 0x8f,
	0x51, 0xd3, 0xad, 0x1e, 0xce, 0x3e, 0xa7, 0x28, 0x67, 0xcb, 0xc9, 0xce, 0x82, 0xe1, 0x0a, 0xea,
	0x01, 0x34, 0xe6, 0xda, 0xc4, 0xf6, 0x0a, 0x61, 0x8b, 0x8a, 0xb5, 0x7a, 0x80, 0x63, 0x80, 0x2b,
	0x3d, 0x62, 0x4f, 0x0b, 0x61, 0x4b, 0x32, 0xd5, 0xde, 0x5b, 0x28, 0x72, 0x0d, 0xf5, 0x53, 0xd8,
	0xf9, 0x8b, 0x66, 0xb1, 0xe7, 0x85, 0x9c, 0xd5, 0x9a, 0xb6, 0x72, 0xbe, 0xd1, 0x26, 0x49, 0xf2,
	0xeb, 0x3f, 0x01, 0x00, 0x00, 0xff, 0xff, 0xe6, 0xdf, 0x1d, 0x10, 0x56, 0x07, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// AccountsUserServiceClient is the client API for AccountsUserService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type AccountsUserServiceClient interface {
	// Create a new user
	CreateUser(ctx context.Context, in *User, opts ...grpc.CallOption) (*UserResponse, error)
	// GetUsersByQuery
	GetUsersByQuery(ctx context.Context, in *User, opts ...grpc.CallOption) (*UsersResponse, error)
	// PatchUser
	PatchUser(ctx context.Context, in *PatchUserRequest, opts ...grpc.CallOption) (*UserResponse, error)
	// DeleteUserRequest
	DeleteUser(ctx context.Context, in *DeleteUserRequest, opts ...grpc.CallOption) (*UserBoolResponse, error)
	// ValidateUserAccount
	ValidateUserAccount(ctx context.Context, in *ValidateUserAccountRequest, opts ...grpc.CallOption) (*UserResponse, error)
}

type accountsUserServiceClient struct {
	cc *grpc.ClientConn
}

func NewAccountsUserServiceClient(cc *grpc.ClientConn) AccountsUserServiceClient {
	return &accountsUserServiceClient{cc}
}

func (c *accountsUserServiceClient) CreateUser(ctx context.Context, in *User, opts ...grpc.CallOption) (*UserResponse, error) {
	out := new(UserResponse)
	err := c.cc.Invoke(ctx, "/secprosvc.AccountsUserService/CreateUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *accountsUserServiceClient) GetUsersByQuery(ctx context.Context, in *User, opts ...grpc.CallOption) (*UsersResponse, error) {
	out := new(UsersResponse)
	err := c.cc.Invoke(ctx, "/secprosvc.AccountsUserService/GetUsersByQuery", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *accountsUserServiceClient) PatchUser(ctx context.Context, in *PatchUserRequest, opts ...grpc.CallOption) (*UserResponse, error) {
	out := new(UserResponse)
	err := c.cc.Invoke(ctx, "/secprosvc.AccountsUserService/PatchUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *accountsUserServiceClient) DeleteUser(ctx context.Context, in *DeleteUserRequest, opts ...grpc.CallOption) (*UserBoolResponse, error) {
	out := new(UserBoolResponse)
	err := c.cc.Invoke(ctx, "/secprosvc.AccountsUserService/DeleteUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *accountsUserServiceClient) ValidateUserAccount(ctx context.Context, in *ValidateUserAccountRequest, opts ...grpc.CallOption) (*UserResponse, error) {
	out := new(UserResponse)
	err := c.cc.Invoke(ctx, "/secprosvc.AccountsUserService/ValidateUserAccount", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AccountsUserServiceServer is the server API for AccountsUserService service.
type AccountsUserServiceServer interface {
	// Create a new user
	CreateUser(context.Context, *User) (*UserResponse, error)
	// GetUsersByQuery
	GetUsersByQuery(context.Context, *User) (*UsersResponse, error)
	// PatchUser
	PatchUser(context.Context, *PatchUserRequest) (*UserResponse, error)
	// DeleteUserRequest
	DeleteUser(context.Context, *DeleteUserRequest) (*UserBoolResponse, error)
	// ValidateUserAccount
	ValidateUserAccount(context.Context, *ValidateUserAccountRequest) (*UserResponse, error)
}

// UnimplementedAccountsUserServiceServer can be embedded to have forward compatible implementations.
type UnimplementedAccountsUserServiceServer struct {
}

func (*UnimplementedAccountsUserServiceServer) CreateUser(ctx context.Context, req *User) (*UserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateUser not implemented")
}
func (*UnimplementedAccountsUserServiceServer) GetUsersByQuery(ctx context.Context, req *User) (*UsersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetUsersByQuery not implemented")
}
func (*UnimplementedAccountsUserServiceServer) PatchUser(ctx context.Context, req *PatchUserRequest) (*UserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method PatchUser not implemented")
}
func (*UnimplementedAccountsUserServiceServer) DeleteUser(ctx context.Context, req *DeleteUserRequest) (*UserBoolResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteUser not implemented")
}
func (*UnimplementedAccountsUserServiceServer) ValidateUserAccount(ctx context.Context, req *ValidateUserAccountRequest) (*UserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ValidateUserAccount not implemented")
}

func RegisterAccountsUserServiceServer(s *grpc.Server, srv AccountsUserServiceServer) {
	s.RegisterService(&_AccountsUserService_serviceDesc, srv)
}

func _AccountsUserService_CreateUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(User)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AccountsUserServiceServer).CreateUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/secprosvc.AccountsUserService/CreateUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AccountsUserServiceServer).CreateUser(ctx, req.(*User))
	}
	return interceptor(ctx, in, info, handler)
}

func _AccountsUserService_GetUsersByQuery_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(User)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AccountsUserServiceServer).GetUsersByQuery(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/secprosvc.AccountsUserService/GetUsersByQuery",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AccountsUserServiceServer).GetUsersByQuery(ctx, req.(*User))
	}
	return interceptor(ctx, in, info, handler)
}

func _AccountsUserService_PatchUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PatchUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AccountsUserServiceServer).PatchUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/secprosvc.AccountsUserService/PatchUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AccountsUserServiceServer).PatchUser(ctx, req.(*PatchUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AccountsUserService_DeleteUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AccountsUserServiceServer).DeleteUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/secprosvc.AccountsUserService/DeleteUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AccountsUserServiceServer).DeleteUser(ctx, req.(*DeleteUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AccountsUserService_ValidateUserAccount_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ValidateUserAccountRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AccountsUserServiceServer).ValidateUserAccount(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/secprosvc.AccountsUserService/ValidateUserAccount",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AccountsUserServiceServer).ValidateUserAccount(ctx, req.(*ValidateUserAccountRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _AccountsUserService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "secprosvc.AccountsUserService",
	HandlerType: (*AccountsUserServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateUser",
			Handler:    _AccountsUserService_CreateUser_Handler,
		},
		{
			MethodName: "GetUsersByQuery",
			Handler:    _AccountsUserService_GetUsersByQuery_Handler,
		},
		{
			MethodName: "PatchUser",
			Handler:    _AccountsUserService_PatchUser_Handler,
		},
		{
			MethodName: "DeleteUser",
			Handler:    _AccountsUserService_DeleteUser_Handler,
		},
		{
			MethodName: "ValidateUserAccount",
			Handler:    _AccountsUserService_ValidateUserAccount_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "protos/accounts-service.proto",
}
